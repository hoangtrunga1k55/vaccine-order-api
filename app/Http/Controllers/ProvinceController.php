<?php

namespace App\Http\Controllers;

use App\Repositories\Contracts\ProvinceRepositoryInterface;
use Illuminate\Http\Request;

class ProvinceController extends Controller
{
    public $provinceRepository;
    public function __construct(ProvinceRepositoryInterface $provinceRepository)
    {
        $this->provinceRepository = $provinceRepository;
    }

    public function getProvinces(){
        return response()->json([
            'success' => true,
            'data' => $this->provinceRepository->all(),
            'message' => ''
        ]);
    }
}
