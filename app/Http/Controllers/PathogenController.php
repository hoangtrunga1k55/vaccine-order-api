<?php

namespace App\Http\Controllers;

use App\Repositories\Contracts\PathogenRepositoryInterface;
use App\Repositories\Eloquents\MedicineLocationRepository;
use Illuminate\Http\Request;

class PathogenController extends Controller
{
    public $pathogenRepository;

    public function __construct(PathogenRepositoryInterface $pathogenRepository)
    {
        $this->pathogenRepository = $pathogenRepository;
    }

    public function getPathogen(){
        return response()->json([
            'success' => true,
            'data' => $this->pathogenRepository->all(),
            'message' => ''
        ]);
    }
}
