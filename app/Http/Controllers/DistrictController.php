<?php

namespace App\Http\Controllers;

use App\Repositories\Contracts\DistrictRepositoryInterface;
use Illuminate\Http\Request;

class DistrictController extends Controller
{
    public $provinceRepository;
    public function __construct(DistrictRepositoryInterface $provinceRepository)
    {
        $this->provinceRepository = $provinceRepository;
    }

    public function getDistricts(Request $request){
        if(isset($request->province_id) && !empty($request->province_id)){
            return response()->json([
                'success' => true,
                'data' => $this->provinceRepository->findAllBy('province_id', $request->province_id),
                'message' => ''
            ]);
        }
        return response()->json([
            'success' => false,
            'message' => 'Missing parameter province_id'
        ]);
    }
}
