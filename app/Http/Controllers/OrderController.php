<?php

namespace App\Http\Controllers;

use App\Models\Order;
use App\Repositories\Contracts\OrderRepositoryInterface;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class OrderController extends Controller
{
    public $orderRepository;

    public function __construct(OrderRepositoryInterface $orderRepository)
    {
        $this->orderRepository = $orderRepository;
    }


    public function storeRegisterInfo(Request $request)
    {
        $regex = '/(^(((0[1-9]|1[0-9]|2[0-8])[\/](0[1-9]|1[012]))|((29|30|31)[\/](0[13578]|1[02]))|((29|30)[\/](0[4,6,9]|11)))[\/](19|[2-9][0-9])\d\d$)|(^29[\/]02[\/](19|[2-9][0-9])(00|04|08|12|16|20|24|28|32|36|40|44|48|52|56|60|64|68|72|76|80|84|88|92|96)$)/';
        $success = true;
        $message = "Tạo đơn đặt hàng thành công";

        $this->validate($request, [
            'full_name' => 'required',
            'gender' => 'required',
            'birth_day' => 'required',
            'code_note' => '',
            'phone_number' => 'required',
            'email' => 'required',
            'address' => 'required',
            'province_id' => 'required',
            'district_id' => 'required',
            'ward_id' => 'required',
            'contact_person_1_name' => 'required',
            'contact_person_1_phone_number' => 'required',
            'contact_person_1_relationship' => 'required',
            'contact_person_2_name' => 'required',
            'contact_person_2_phone_number' => 'required',
            'contact_person_2_relationship' => 'required',
            'type_injection' => 'required',
            'pathogen_id' => 'required',
            'location_medicine_id' => 'required',
            'booking_date' => "required",
        ]);

        $mc_code = "AMV";
        $id = $this->getId();
        $order_id = $mc_code . Carbon::now()->format("Ymd") . str_pad($id, 7, "0", STR_PAD_LEFT);

        //Handle create order
        $order_attributes = [
            //Dữ liệu gen tự động
            'order_id' => $order_id,
            'expired_at' => (int)Carbon::now()->addMinutes(10)->timestamp,

            //Dữ liệu từ form
            'full_name' => $request->full_name,
            'gender' => $request->gender,
            'birth_day' => $request->birth_day,
            'phone_number' => $request->phone_number,
            'email' => $request->email,
            'address' => $request->address,
            'province_id' => $request->province_id,
            'district_id' => $request->district_id,
            'ward_id' => $request->ward_id,
            'location_medical' => $request->location_medical,

            //Dữ liệu fix cứng tạm thời
            'quantity' => 1,
            'amount' => "500000",
            'currency' => "VND",
            'client' => "web",
            'order_type' => "BOOK_INJECT",
            'sub_order_type' => "",
            'source_of_fund' => "BANK_ATM",
            'channel' => "",
            'mc_code' => $mc_code,

            //Dữ liệu fix cứng
            'status' => Order::STATUS_INITIAL,
            'state' => Order::STATE_APPLIED,
            'reason_cancel' => "",
            'error_code' => ""

        ];

        //Handle create order info
        $order_info_attributes = [
            //Dữ liệu gen tự động
            'order_id' => $order_id,

            //Dữ liệu từ form
            'contact_person_1_name' => $request->contact_person_1_name,
            'contact_person_1_phone_number' => $request->contact_person_1_phone_number,
            'contact_person_1_relationship' => $request->contact_person_1_relationship,
            'contact_person_2_name' => $request->contact_person_2_name,
            'contact_person_2_phone_number' => $request->contact_person_2_phone_number,
            'contact_person_2_relationship' => $request->contact_person_2_relationship,
            'type_injection' => $request->type_injection,
            'code_note' => $request->code_note,
            'pathogen_id' => $request->pathogen_id,
            'booking_date' => $request->booking_date,
            'location_medical' => $request->location_medical
        ];
        if (preg_match($regex, $request->booking_date)) {
            $result = $this->createOrderInfo($order_info_attributes, $mc_code);
            if (!$result['success']) {
                $message = $result['message'];
            }
        } else {
            $message = "Định dạng ngày tiêm không hợp lệ";
        }
        $order_attributes['errorMsg'] = $message;

        $this->orderRepository->insert($order_attributes);

        return response()->json([
            'success' => $success,
            'message' => $message
        ]);
    }


    public function createOrderInfo($attribute, $mc_code)
    {
        try {
            DB::table('order_info_' . $mc_code)
                ->insertGetId($attribute);
        } catch (\Exception $exception) {
            return [
                'success' => false,
                'message' => $exception->getMessage()
            ];
        }
        return [
            'success' => true,
        ];
    }

    public function getId()
    {
        $last = DB::table('order_auto_increment_id')
            ->where("date", "like", Carbon::now()->format("Ymd"))
            ->get()
            ->last();
        if (is_null($last)) {
            DB::table('order_auto_increment_id')
                ->insert([
                    'date' => Carbon::now()->format("Ymd"),
                    'AI_id' => 1
                ]);
            return 1;
        } else {
            DB::table('order_auto_increment_id')
                ->insert([
                    'date' => Carbon::now()->format("Ymd"),
                    'AI_id' => $last['AI_id'] + 1
                ]);
            return $last['AI_id'] + 1;
        }
    }

    public function delete()
    {
        DB::table('orders')->delete();
        DB::table('order_info_AMV')->delete();
        DB::table('order_auto_increment_id')->delete();
        die;
    }
}
