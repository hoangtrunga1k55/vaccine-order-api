<?php

namespace App\Http\Controllers;

use App\Repositories\Contracts\WardRepositoryInterface;
use Illuminate\Http\Request;

class WardController extends Controller
{
    public $wardRepository;

    public function __construct(WardRepositoryInterface $wardRepository)
    {
        $this->wardRepository = $wardRepository;
    }

    public function getWards(Request $request)
    {
        if (isset($request->district_id) && !empty($request->district_id)) {
            return response()->json([
                'success' => true,
                'data' => $this->wardRepository->findAllBy('district_id', $request->district_id),
                'message' => ''
            ]);
        }
        return response()->json([
            'success' => false,
            'message' => 'Missing parameter district_id'
        ]);
    }
    //
}
