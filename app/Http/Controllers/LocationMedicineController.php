<?php

namespace App\Http\Controllers;

use App\Repositories\Contracts\MedicineLocationRepositoryInterface;
use App\Repositories\Eloquents\MedicineLocationRepository;
use Illuminate\Http\Request;

class LocationMedicineController extends Controller
{
    public $medicineLocationRepository;

    public function __construct(MedicineLocationRepositoryInterface $medicineLocationRepository)
    {
        $this->medicineLocationRepository = $medicineLocationRepository;
    }

    public function getMedicineLocation(){
        return response()->json([
            'success' => true,
            'data' => $this->medicineLocationRepository->all(),
            'message' => ''
        ]);
    }
}
