<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
    public function boot()
    {
        $models = [
            "CmsAccount",
            "PasswordReset",
            "Log",
            "Config",
            "Permission",
            "Role",
            "News",
            "Category",
            "Vendor",
            "Notification",
            "Vendor",
            "Pathogen",
            "MedicineLocation",
            "Vaccine",
            "Tag",
            "Popup",
            "VaccineBook",
            "User",
            "Province",
            "Ward",
            "District",
            "Order"
        ];

        foreach ($models as $model) {
            $this->app->bind('App\Repositories\Contracts\\'. $model .'RepositoryInterface', 'App\Repositories\Eloquents\\'. $model .'Repository');
        }
    }
}
