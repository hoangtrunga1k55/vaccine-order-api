<?php

namespace App\Models;

class MedicineLocation extends MongoModel
{
    protected $collection = 'location_medicine';
    protected $guarded = [];

}
