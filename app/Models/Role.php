<?php

namespace App\Models;

class Role extends MongoModel
{
    protected $collection = 'roles';
    protected $guarded = [];


    public function cmsAccounts()
    {
        return $this->belongsToMany(
            CmsAccount::class
        );
    }

}
