<?php

namespace App\Models\WalletCore;

class UserBalance extends MongoModel
{
    protected $collection = 'user_balances';

    protected $fillable = ['processing_id', 'amount_available', 'amount_freeze', 'pending_transactions', 'user_id', 'balance_id'];
}
