<?php


namespace App\Models;


class PasswordReset extends MongoModel
{
    protected $collection = 'password_resets';
    protected $guarded = [];
}
