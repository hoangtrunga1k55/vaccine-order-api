<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\MustVerifyEmail;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class CmsAccount extends MongoModel implements
    AuthenticatableContract,
    AuthorizableContract,
    CanResetPasswordContract
{
    use Authenticatable, CanResetPassword, MustVerifyEmail;

    protected $primaryKey = '_id';
    protected $collection = 'cms_accounts';
    protected $guarded = [];

    public function roles()
    {
        return $this->belongsToMany(
            Role::class
        );
    }

    public function getStatus()
    {
        switch ($this->status) {
            case "0":
                return "Ngừng hoạt động";
            case "1":
                return "Đang hoạt động";
        }
    }

    public function getBadgeColor()
    {
        switch ($this->status) {
            case "0":
                return "danger";
            case "1":
                return "success";
        }
    }

    public function can($abilities, $arguments = [])
    {
        // TODO: Implement can() method.
    }
}
