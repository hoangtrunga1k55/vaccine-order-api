<?php

namespace App\Models\Wallet;

class BankAccount extends MongoModel
{
    //
    protected $collection = 'bank_accounts';

//    protected $primaryKey = '_id';

    protected $fillable = [
        '_id',
        'user_id',
        'bank_id',
        'name',
        'number',
        'branch',
        'can_update',
        'is_default',
        'status',
        '__v'
    ];

    public function bank()
    {
        return $this->belongsTo('App\Models\Wallet\Bank','bank_id');
    }
}
