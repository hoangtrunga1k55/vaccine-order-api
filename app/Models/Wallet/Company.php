<?php

namespace App\Models\Wallet;

class Company extends MongoModel
{
    protected $collection = 'companies';
    
//    protected $primaryKey = '_id';

    protected $fillable = ['_id', 'name', 'pending_transactions', 'notification_url', 'verify_otp_url', 'core_balance_id', 'core_id', 'status', 'public_key', 'private_key'];

    public function balances(){
        return $this->hasOne('App\Models\WalletCore\UserBalance', 'user_id', 'core_id');
    }

}
