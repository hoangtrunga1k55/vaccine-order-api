<?php

namespace App\Models\Wallet;

use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Foundation\Auth\Access\Authorizable;

class MerchantAccount extends MongoModel
{
    protected $collection = 'merchant_accounts';

    protected $fillable = ['_id', 'email', 'is_two_factor', 'password', 'secret_key', 'merchant_id', '__v', 'last_login', 'salt','status','name','phone_number','address','website','logo','limited','user_id'];

    public function user()
    {
        return $this->belongsTo('App\Models\Wallet\User','user_id');
    }
}
