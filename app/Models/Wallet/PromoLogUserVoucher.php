<?php

namespace App\Models\Wallet;

use Illuminate\Database\Eloquent\Model;

class PromoLogUserVoucher extends MongoModel
{
    protected $collection = 'promo_log_user_voucher';

    protected $fillable = [
        '_id',
        'id_user',
        'action',
        'state',
        'id_vouchers'
    ];
    public function voucher()
    {
        return $this->belongsTo('App\Models\Wallet\PromoVoucher','id_voucher', '_id');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\Wallet\User','id_user', '_id');
    }
}
