<?php

namespace App\Models\Wallet;

class CmsLog extends MongoModel
{
    protected $collection = 'cms_logs';
    
    protected $fillable = [
        '_id',
        'cms_account_email',
        'cms_type',
        'route',
        'args',
        '__v'
    ];
}
