<?php

namespace App\Models\Wallet;

class Fee extends MongoModel
{
    //
    protected $table = 'fee';

    protected $fillable = [
        'fee_transaction',
        'type_transaction',
        'max_amount',
        'status',
        'min_amount',
        'apply_for_user',
        'weight_model',
        'time_apply_fee'
    ];
}
