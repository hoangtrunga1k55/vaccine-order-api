<?php

namespace App\Models\Wallet;

class Config extends MongoModel
{
    //
    protected $table = 'configs';

    protected $fillable = [
        'firebase_key',
        'vnpay_code',
        'vnpay_url_payment',
        'vnpay_url_return',
        'vnpay_hash_secret',
        'hash_api_key',
        'hash_access_token',
        'hash_refresh_token',
        'hash_password',
        'merchant_phone_account'
    ];
}
