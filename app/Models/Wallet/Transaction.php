<?php

namespace App\Models\Wallet;

class Transaction extends MongoModel
{
    protected $collection = 'transactions';
    
//    protected $primaryKey = '_id';

    protected $fillable = [
        '_id',
        'device_id',
        'is_system_deposit',
        'payer_freeze_money',
        'payer_release_money',
        'payee_deposit_money',
        'company_freeze_money',
        'company_release_money',
        'date_time',
        'state',
        'status',
        'amount',
        'currency',
        'company_id',
        'company_core_id',
        'company_balance_core_id',
        'company_freeze_core_id',
        'type',
        'message'
    ];

    public function payee()
    {
        return $this->belongsTo('App\Models\Wallet\User','payee_id');
    }

    public function payer()
    {
        return $this->belongsTo('App\Models\Wallet\User','payer_id');
    }

    public function company()
    {
        return $this->belongsTo('App\Models\Wallet\Company','company_id');
    }

    public function gutina_log()
    {
        return $this->hasOne('App\Models\Wallet\GutinaLog','transid');
    }

    public function merchant_invoice()
    {
        return $this->hasOne('App\Models\Wallet\MerchantInvoice','invoice_id', 'invoice_id');
    }

}
