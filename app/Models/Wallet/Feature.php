<?php

namespace App\Models\Wallet;

class Feature extends MongoModel
{
    protected $collection = 'features';
    
//    protected $primaryKey = '_id';

    protected $fillable = [
        '_id',
        'available_versions',
        'available_os',
        'name',
        'is_all_os',
        'is_all_version',
        'icon',
        'uri',
        'priority',
        'status',
        'is_home',
        '__v',
//        'need_translate',
//        'translates'
    ];
}
