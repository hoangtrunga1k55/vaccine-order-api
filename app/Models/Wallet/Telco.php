<?php

namespace App\Models\Wallet;

class Telco extends MongoModel
{
    protected $collection = 'telecommunications_company';
    
//    protected $primaryKey = '_id';

    protected $fillable = [
        '_id',
        'telco_name',
        'logo',
        'logo_2',
        'card_type',
        'prices',
        'type',
        'status',
        'priority',
        '__v'
    ];
}
