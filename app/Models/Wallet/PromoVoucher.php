<?php

namespace App\Models\Wallet;

use Jenssegers\Mongodb\Eloquent\SoftDeletes;

class PromoVoucher extends MongoModel
{
    use SoftDeletes;

    protected $collection = 'promo_voucher';
    
    protected $fillable = [
        '_id',
        'title',
        'thumbnail',
        'rule',
//        'description',
        'voucher_code',
        'amount',
        'type',
        'discount_amount',
        'discount_percent',
        'merchant_id',
        'max_discount_amount',
        'status',
        'started_at',
        'expired_at',
        'show_in_my_voucher',
        'show',
        'total',
        'short_description',
        'total_can_buy_per_day',
        'total_can_buy',
        'total_can_use_per_day',
        'total_can_use',
        'merchant_id',
        '__v'
    ];

    public static function getListType()
    {
        return [
            "TOPUP"                   => "TOPUP",
            "BUYCARD"                 => "BUYCARD",
            "TRANSFER_GUTINA"         => "TRANSFER_GUTINA",
            "WITHDRAW_GUTINA"         => "WITHDRAW_GUTINA",
            "TRANSFER"                => "TRANSFER",
            "BALANCE_CHANGE_DEPOSIT"  => "BALANCE_CHANGE_DEPOSIT",
            "BALANCE_CHANGE_WITHDRAW" => "BALANCE_CHANGE_WITHDRAW",
        ];
    }
}
