<?php

namespace App\Models\Wallet;

class Message extends MongoModel
{
    protected $collection = 'messages';
    
//    protected $primaryKey = '_id';
    protected $fillable = [
        'title',
        'content',
        'type',
        'external_url',
        'message_topic_id',
        'user_id',
        'group_id',
        'type_push',
        'topic',
        'time_push',
        'image_url',
        'description',
        'status_push',
        'noti_type',
        'noti_id'
    ];

    public const TYPE = [
        'TRANSACTION_DETAIL',
        'GUTINA_ORDER_DETAIL' ,
        'PROMOTION_DETAIL',
        'NOTIFICATION_DETAIL',
    ];

    public function message_topic()
    {
        return $this->belongsTo('App\Models\Wallet\MessageTopic', 'group_id', '_id');
    }
}
