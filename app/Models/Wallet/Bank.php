<?php

namespace App\Models\Wallet;

class Bank extends MongoModel
{
    protected $collection = 'banks';
    
//    protected $primaryKey = '_id';

    protected $fillable = ['_id', 'name', 'logo', 'short_name', 'description', 'ibanking_url', 'app_ios_schema_url', 'app_android_schema_url', 'type', 'status', 'sort_order'];
}
