<?php

namespace App\Models\Wallet;

class MessageTopic extends MongoModel
{
    protected $collection = 'message_topics';

    protected $fillable = [
        'name',
        'code',
        'user_ids',
//        'type',
        'description',
        'total_device'
    ];
}
