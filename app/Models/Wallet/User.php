<?php

namespace App\Models\Wallet;

class User extends MongoModel {

    protected $collection = 'users';

//    protected $primaryKey = '_id';

    protected $fillable = [
        '_id',
        'name',
        'phone_number',
        'email',
        'language',
        'currency',
        'role',
        'status',
        'can_add_bank_account',
        'can_update_name',
        'can_update_email',
        'can_update_phone_number',
        'address',
        'is_facebook_connect',
        'identity_number',
        'gender',
        'is_banned',
        'time_banned'
    ];

    public function balancePrimary()
    {
        return $this->hasMany('App\Models\WalletCore\UserBalance', 'user_id', 'core_id')->where('balance_id', 'PRIMARY');
    }

    public function balances()
    {
        return $this->hasMany('App\Models\WalletCore\UserBalance', 'user_id', 'core_id');
    }

}
