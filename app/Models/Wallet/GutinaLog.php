<?php

namespace App\Models\Wallet;

use App\Repositories\Eloquents\GutinaLogRepository;

class GutinaLog extends MongoModel
{
    protected $collection = 'gutina_log';

    public static function getListType()
    {
        return [
            0 => 'Tất cả các loại đơn',
            GutinaLogRepository::TYPE_ACCOUNT_TRANSFER => 'Đơn hàng thu TM - nộp CK',
            GutinaLogRepository::TYPE_CASH_TRANSFER => 'Đơn hàng TM-TM',
            GutinaLogRepository::TYPE_CARD => 'Đơn hàng thẻ cào',
            GutinaLogRepository::TYPE_BANK_TRANSFER => 'Đơn CK-CK',
            GutinaLogRepository::TYPE_WITHDRAW => 'Đơn hàng chuyển khoản - thu TM',
        ];
    }

    public function typeTitle()
    {
        $types = self::getListType();
        return isset($types[$this->logs['type']]) ? $types[$this->logs['type']] : $this->logs['type'];
    }

    public function transaction()
    {
        return $this->belongsTo('App\Models\Wallet\Transaction','transid');
    }

    public function createTitle()
    {
        return $this->logs['userfullname'] ? $this->logs['userfullname'] : '';
    }

    public function getCancelTitle()
    {
        switch ($this->logs->cancel) {
            case GutinaLogRepository::CANCEL_BY_AGENT:
                return 'Shipper hủy đơn';
                break;
            case GutinaLogRepository::CANCEL_BY_USER:
                return 'Khách hủy đơn';
                break;
            case GutinaLogRepository::CANCEL_BY_COORDINATOR:
                return 'Điều phối hủy đơn';
                break;
            case GutinaLogRepository::CANCEL_BY_ACCOUNTANT:
                return 'Kế toán hủy đơn';
                break;
            case GutinaLogRepository::CANCEL_BY_TIMEOUT:
                return 'Không tìm được shipper';
                break;
            default:
                return 'Khác';
                break;
        }
    }

    public function getCurrentStep($status = null, $listStep = [])
    {
        if (is_null($status)) {
            $status = $this->logs['status'];
        }

        $step = 1;
        switch ($this->logs['type']) {
            case GutinaLogRepository::TYPE_WITHDRAW:
                if (isset($this->logs['requireagent']) && $this->logs['requireagent'] == 1) {
                    $list_status = [
                        GutinaLogRepository::STATUS_AGENT_CALLED,
                        GutinaLogRepository::STATUS_AGENT_ACCEPTED,
                        GutinaLogRepository::STATUS_AGENT_DO_PAYMENT,
                        GutinaLogRepository::STATUS_AGENT_PAID,
                        GutinaLogRepository::STATUS_DELIVERED,
                    ];

                    $transfer_time = !empty($this->logs['transfertime']) ? true : false;
                    $accepted_at = !empty($this->log['acceptedat']) ? true : false;

                    if (((in_array($this->logs['status'], $list_status) && !$transfer_time && !$accepted_at) ||
                        ($transfer_time && $accepted_at && ($this->logs['transfertime'] > $this->logs['acceptedat'])) || ($accepted_at && !$transfer_time))) {

                        switch ($status) {
                            case GutinaLogRepository::STATUS_NEW:
                                $step = 1;
                                break;
                            case GutinaLogRepository::STATUS_AGENT_CALLED:
                                $step = 2;
                                break;
                            case GutinaLogRepository::STATUS_AGENT_ACCEPTED:
                                $step = 3;
                                break;
                            case GutinaLogRepository::STATUS_WAIT_USER_TRANSFER:
                            case GutinaLogRepository::STATUS_USER_TRANSFERRED:
                                $step = 4;
                                break;
                            case GutinaLogRepository::STATUS_DELIVERED:
                            case GutinaLogRepository::STATUS_QR_CODE_CREATED:
                                $step = 5;
                                break;
                            case GutinaLogRepository::STATUS_COMPLETED:
                            case GutinaLogRepository::STATUS_CANCELLED:
                                $step = 6;
                                break;
                        }

                    } else {
                        switch ($status) {
                            case GutinaLogRepository::STATUS_NEW:
                                $step = 1;
                                break;
                            case GutinaLogRepository::STATUS_COMPLETED:
                                $step = 6;
                                break;
                            case GutinaLogRepository::STATUS_AGENT_CALLED:
                                $step = 4;
                                break;
                            case GutinaLogRepository::STATUS_AGENT_ACCEPTED:
                                $step = 4;
                                break;
                            case GutinaLogRepository::STATUS_QR_CODE_CREATED:
                                $step = 5;
                                break;
                            case GutinaLogRepository::STATUS_CANCELLED:
                                $step = 6;
                                break;
                            case GutinaLogRepository::STATUS_WAIT_USER_TRANSFER:
                                $step = 2;
                                break;
                            case GutinaLogRepository::STATUS_USER_TRANSFERRED:
                                $step = 3;
                                break;
                        }
                    }

                } else {
                    switch ($status) {
                        case GutinaLogRepository::STATUS_NEW:
                            $step = 1;
                            break;
                        case GutinaLogRepository::STATUS_COMPLETED:
                            $step = 4;
                            break;
                        case GutinaLogRepository::STATUS_CANCELLED:
                            $step = 4;
                            break;
                        case GutinaLogRepository::STATUS_WAIT_USER_TRANSFER:
                            $step = 2;
                            break;
                        case GutinaLogRepository::STATUS_USER_TRANSFERRED:
                            $step = 3;
                            break;
                    }
                }
                break;
            case GutinaLogRepository::TYPE_BANK_TRANSFER:
                switch ($status) {
                    case GutinaLogRepository::STATUS_NEW:
                        $step = 1;
                        break;
                    case GutinaLogRepository::STATUS_COMPLETED:
                        $step = 3;
                        break;
                    case GutinaLogRepository::STATUS_CANCELLED:
                        $step = 3;
                        break;
                    case GutinaLogRepository::STATUS_USER_TRANSFERRED:
                        $step = 2;
                        break;
                }
                break;
            default:
                if (isset($this->logs['requireagent']) && $this->logs['requireagent'] == 1) {
                    if (in_array($this->logs['agenttype'], [GutinaLogRepository::AGENT_TYPE_FIXED, GutinaLogRepository::AGENT_TYPE_MOBILE]) ||
                        (!empty($this->logs['agentphone']) && in_array($this->logs['agenttype'], [2, 3])))
                    {
                        switch ($status) {
                            case GutinaLogRepository::STATUS_NEW:
                                $step = 1;
                                break;
                            case GutinaLogRepository::STATUS_COMPLETED:
                                $step = 6;
                                break;
                            case GutinaLogRepository::STATUS_AGENT_CALLED:
                                $step = 2;
                                break;
                            case GutinaLogRepository::STATUS_AGENT_ACCEPTED:
                                $step = 3;
                                break;
                            case GutinaLogRepository::STATUS_QR_CODE_CREATED:
                                $step = 4;
                                break;
                            case GutinaLogRepository::STATUS_AGENT_RECEIVED:
                                $step = 5;
                                break;
                            /*case OrderRepository::STATUS_AGENT_TRANSFERRED:
                                $step = 6;
                                break;*/
                            case GutinaLogRepository::STATUS_CANCELLED:
                                $step = 6;
                                break;
                        }
                    } else {
                        switch ($status) {
                            case GutinaLogRepository::STATUS_NEW:
                                $step = 1;
                                break;
                            case GutinaLogRepository::STATUS_COMPLETED:
                                $step = 6;
                                break;
                            case GutinaLogRepository::STATUS_AGENT_CALLED:
                                $step = 2;
                                break;
                            case GutinaLogRepository::STATUS_AGENT_ACCEPTED:
                                $step = 3;
                                break;
                            case GutinaLogRepository::STATUS_QR_CODE_CREATED:
                                $step = 4;
                                break;
                            case GutinaLogRepository::STATUS_AGENT_RECEIVED:
                                $step = 5;
                                break;
                            case GutinaLogRepository::STATUS_CANCELLED:
                                $step = 6;
                                break;
                        }
                    }
                } else {
                    switch ($status) {
                        case GutinaLogRepository::STATUS_NEW:
                            $step = 1;
                            break;
                        case GutinaLogRepository::STATUS_AGENT_RECEIVED:
                            $step = 2;
                            break;
                        case GutinaLogRepository::STATUS_CANCELLED:
                        case GutinaLogRepository::STATUS_COMPLETED:
                            $step = 3;
                            break;
                    }
                }
                break;
        }

        if (empty($listStep)) {
            $listStep = $this->getListStep(false);
        }

        return [
            'index' => $step,
            'title' => isset($listStep[$step]) ? $listStep[$step]['title'] : ''
        ];
    }

    /**
     * @param bool $checkStatus
     * @return array
     */
    public function getListStep($checkStatus = true)
    {
        $finalTitle = 'Hoàn thành';
        $finalDesc = '';
        if ($this->logs['status'] == GutinaLogRepository::STATUS_COMPLETED) {
            $finalDesc = 'Đơn hàng thành công<br>' . (!empty($this->updated_at) ? $this->updated_at->format('H:i:s d/m/Y') : '');
        }
        if ($this->logs['status'] == GutinaLogRepository::STATUS_CANCELLED) {
            switch ($this->logs['cancel']) {
                case GutinaLogRepository::CANCEL_BY_AGENT:
                    $finalTitle = 'Agent huỷ đơn';
                    break;
                case GutinaLogRepository::CANCEL_BY_USER:
                    $finalTitle = 'KH hủy đơn';
                    break;
                case GutinaLogRepository::CANCEL_BY_COORDINATOR:
                    $finalTitle = 'Điều phổi hủy đơn';
                    break;
                case GutinaLogRepository::CANCEL_BY_ACCOUNTANT:
                    $finalTitle = 'Kế toán hủy đơn';
                    break;
                case GutinaLogRepository::CANCEL_BY_TIMEOUT:
                    $finalTitle = 'Không tìm được shipper';
                    break;
                default:
                    $finalTitle = 'Đã hủy';
                    break;
            }
            $finalDesc = $this->logs['cancelreason'] . '<br>' . $finalDesc;
        }

        switch ($this->logs['type']) {
            case GutinaLogRepository::TYPE_WITHDRAW:
                /*if ($this->company_id > 0) {
                    return [
                        1 => [
                            'index' => 1,
                            'title' => $this->created_by == OrderRepository::CREATE_BY_CMS ? 'Điều phối viên tạo đơn' : 'KH tạo đơn',
                            'desc'  => $this->createTitle() . '<br>' . date('H:i d/m/Y', strtotime($this->created_at))
                        ],
                        2 => [
                            'index' => 2,
                            'title' => 'Tìm shipper',
                            'desc'  => is_null($this->agent) ? '' : $this->agent->getOrderDisplayTitle()
                        ],
                        3 => [
                            'index' => 3,
                            'title' => 'Đang c.khoản',
                            'desc'  => ''
                        ],
                        4 => [
                            'index' => 4,
                            'title' => 'Đã c.khoản',
                            'desc'  => ''
                        ],

                        5 => [
                            'index' => 5,
                            'title' => 'Chờ KH quét QRCode',
                            'desc'  => ''
                        ],
                        6 => [
                            'index' => 6,
                            'title' => $finalTitle,
                            'desc'  => $finalDesc
                        ]KH hủy đơn
                    ];
                }*/
                if (isset($this->logs['requireagent']) && $this->logs['requireagent'] == 1) {
                    $list_status = [
                        GutinaLogRepository::STATUS_AGENT_CALLED,
                        GutinaLogRepository::STATUS_AGENT_ACCEPTED,
                        GutinaLogRepository::STATUS_AGENT_DO_PAYMENT,
                        GutinaLogRepository::STATUS_AGENT_PAID,
                        GutinaLogRepository::STATUS_DELIVERED,
                    ];

                    $transfer_time = !empty($this->logs['transfertime']) ? true : false;
                    $accepted_at = !empty($this->logs['acceptedat']) ? true : false;

                    if (((in_array($this->logs['status'], $list_status) && !$transfer_time && !$accepted_at) ||
                        ( $transfer_time && $accepted_at && ($this->logs['transfertime'] > $this->logs['acceptedat'])) || ($accepted_at && !$transfer_time))) {
                        $steps = [
                            1 => [
                                'index' => 1,
                                'title' => 'KH tạo đơn',
                                'icon' => '<i class="fa fa-mobile" aria-hidden="true"></i>',
                                'desc'  => $this->createTitle() . '<br>' . date('H:i:s d/m/Y', $this->logs['createdat'])
                            ],
                            2 => [
                                'index' => 2,
                                'title' => 'Tìm shipper',
                                'icon' => '',
                                'desc'  => ''
                            ],
                            3 => [
                                'index' => 3,
                                'title' => 'Shipper nhận đơn',
                                'icon' => '',
                                'desc'  => !empty($this->logs['agentfullname']) ? !$this->logs['agentfullname'] : ''
                            ],
                            4 => [
                                'index' => 4,
                                'title' => 'Chờ khách hàng chuyển khoản',
                                'icon' => '',
                                'desc'  => $this->logs['status'] == GutinaLogRepository::STATUS_USER_TRANSFERRED && !empty($this->logs['agentphone']) ? 'Khách hàng đã chuyển khoản' : ''
                            ],
                            5 => [
                                'index' => 5,
                                'title' => 'Chờ khách hàng quét QR',
                                'icon' => '',
                                'desc'  => ''
                            ],
                            6 => [
                                'index' => 6,
                                'title' => $finalTitle,
                                'icon' => '',
                                'desc'  => $finalDesc
                            ]
                        ];

                    } else {
                        $steps = [
                            1 => [
                                'index' => 1,
                                'title' => 'KH tạo đơn',
                                'icon' => '<i class="fa fa-mobile" aria-hidden="true"></i>',
                                'desc'  => $this->createTitle() . '<br>' . date('H:i:s d/m/Y', $this->logs['createdat'])
                            ],
                            2 => [
                                'index' => 2,
                                'title' => 'Đang c.khoản',
                                'icon' => '',
                                'desc'  => ''
                            ],
                            3 => [
                                'index' => 3,
                                'title' => 'Đã c.khoản',
                                'icon' => '',
                                'desc'  => ''
                            ],
                            4 => [
                                'index' => 4,
                                'title' => 'Tìm shipper',
                                'icon' => '',
                                'desc'  => !empty($this->agent) ? $this->agent->getOrderDisplayTitle() : ''
                            ],
                            5 => [
                                'index' => 5,
                                'title' => 'Chờ KH quét QRCode',
                                'icon' => '',
                                'desc'  => ''
                            ],
                            6 => [
                                'index' => 6,
                                'title' => $finalTitle,
                                'icon' => '',
                                'desc'  => $finalDesc
                            ]
                        ];
                    }

                } else {
                    $steps = [
                        1 => [
                            'index' => 1,
                            'title' => 'KH tạo đơn',
                            'icon' => '<i class="fa fa-mobile" aria-hidden="true"></i>',
                            'desc'  => $this->createTitle() . '<br>' . date('H:i:s d/m/Y', $this->logs['createdat'])
                        ],
                        2 => [
                            'index' => 2,
                            'title' => 'Đang c.khoản',
                            'icon' => '',
                            'desc'  => ''
                        ],
                        3 => [
                            'index' => 3,
                            'title' => 'Đã c.khoản',
                            'icon' => '',
                            'desc'  => ''
                        ],
                        4 => [
                            'index' => 4,
                            'title' => $finalTitle,
                            'icon' => '',
                            'desc'  => $finalDesc
                        ]
                    ];
                }
                break;
            default:
                if (isset($this->logs['requireagent']) && $this->logs['requireagent'] == 1) {
                    if (in_array($this->logs['agenttype'], [GutinaLogRepository::AGENT_TYPE_FIXED, GutinaLogRepository::AGENT_TYPE_MOBILE]) ||
                        (!empty($this->logs['agentphone']) && in_array($this->logs['agenttype'], [2, 3]))) {
                        $steps = [
                            1 => [
                                'index' => 1,
                                'title' => 'KH tạo đơn',
                                'icon' => '<i class="fa fa-mobile" aria-hidden="true"></i>',
                                'desc'  => $this->createTitle() . '<br>' . date('H:i:s d/m/Y', ($this->logs['createdat']))
                            ],
                            2 => [
                                'index' => 2,
                                'title' => 'Tìm shipper',
                                'icon' => '',
                                'desc'  => !empty($this->logs['agentfullname']) ? $this->logs['agentfullname'] : ''
                            ],
                            3 => [
                                'index' => 3,
                                'title' => 'Shipper đã nhận đơn',
                                'icon' => '',
                                'desc'  => ''
                            ],
                            4 => [
                                'index' => 4,
                                'title' => 'Shipper đã tới chỗ KH',
                                'icon' => '',
                                'desc'  => 'Chờ KH quét QRCode'
                            ],
                            5 => [
                                'index' => 5,
                                'title' => 'Shipper đã nhận tiền',
                                'icon' => '',
                                'desc'  => 'Đã hoàn tất việc nhận tiền của user'
                            ],
                            /*6 => [
                                'index' => 6,
                                'title' => 'Đại lý đã chuyển tiền',
                                'icon' => '',
                                'desc'  => 'Đại lý đã chuyển tiền vào tk GPay'
                            ],*/
                            6 => [
                                'index' => 6,
                                'title' => $finalTitle,
                                'icon' => '',
                                'desc'  => $finalDesc
                            ]
                        ];
                    } else {
                        $steps = [
                            1 => [
                                'index' => 1,
                                'title' => 'KH tạo đơn',
                                'icon' => '<i class="fa fa-mobile" aria-hidden="true"></i>',
                                'desc'  => $this->createTitle() . '<br>' . date('H:i:s d/m/Y', $this->createdat)
                            ],
                            2 => [
                                'index' => 2,
                                'title' => 'Tìm shipper',
                                'icon' => '',
                                'desc'  => !empty($this->logs['agentfullname']) ? $this->logs['agentfullname'] : ''
                            ],
                            3 => [
                                'index' => 3,
                                'title' => 'Shipper đã nhận đơn',
                                'icon' => '',
                                'desc'  => ''
                            ],
                            4 => [
                                'index' => 4,
                                'title' => 'Shipper đã tới chỗ KH',
                                'icon' => '',
                                'desc'  => 'Chờ KH quét QRCode'
                            ],
                            5 => [
                                'index' => 5,
                                'title' => 'Shipper đã nhận tiền',
                                'icon' => '',
                                'desc'  => 'Đã hoàn tất việc nhận tiền của user'
                            ],
                            6 => [
                                'index' => 6,
                                'title' => $finalTitle,
                                'icon' => '',
                                'desc'  => $finalDesc
                            ]
                        ];
                    }
                } else {
                    $steps = [
                        1 => [
                            'index' => 1,
                            'title' => 'KH tạo đơn',
                            'icon' => '<i class="fa fa-mobile" aria-hidden="true"></i>',
                            'desc'  => $this->createTitle() . '<br>' . date('H:i:s d/m/Y', $this->logs['createdat'])
                        ],
                        2 => [
                            'index' => 2,
                            'title' => 'Đã nhận tiền',
                            'icon' => '',
                            'desc'  => 'Đã hoàn tất việc nhận tiền của user'
                        ],
                        3 => [
                            'index' => 3,
                            'title' => $finalTitle,
                            'icon' => '',
                            'desc'  => $finalDesc
                        ]
                    ];
                }
                break;
        }

        if (!empty($steps)) {
            if ($checkStatus && $this->logs['status'] == GutinaLogRepository::STATUS_CANCELLED) {
                $lastStep = end($steps);
                // Get pre step
                $last_status = !empty($this->logs['laststatus']) ? $this->logs['laststatus'] : 1;
                $preStep = $this->getCurrentStep($last_status, $steps);

                foreach ($steps as $index => $step) {
                    if ($step['index'] > $preStep['index'] && $step['index'] < $lastStep['index']) {
                        unset($steps[$index]);
                    }
                }
                $steps[key($steps)]['show_index'] = false;
            }

            return $steps;

        }

        return [];
    }
}
