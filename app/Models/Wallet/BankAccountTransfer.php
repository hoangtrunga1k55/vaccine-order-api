<?php

namespace App\Models\Wallet;

class BankAccountTransfer extends MongoModel
{
    protected $collection = 'bank_account_transfers';
    
//    protected $primaryKey = '_id';

    protected $fillable = ['_id', 'bank_id', 'bank_account_name', 'bank_account_number', 'bank_account_branch', 'status'];

    public function bank()
    {
        return $this->belongsTo('App\Models\Wallet\Bank','bank_id');
    }
}
