<?php

namespace App\Models;

class Permission extends MongoModel
{
    protected $collection = 'permissions';
    protected $guarded = [];

    public function role()
    {
        return $this->belongsTo(Role::class);
    }
}
