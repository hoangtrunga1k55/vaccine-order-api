<?php

namespace App\Models;

class Vaccine extends MongoModel
{
    protected $collection = 'vaccine';

    protected $fillable = [
        '_id',
        'name',
        'vaccine_code',
        'avatar',
        'turn_inject',
        'ages',
        'day_age',
        'week_age',
        'month_age',
        'fulltext',
        'brand_name',
        'pathogen_ids',
        'creator_id',
        'time_turn_1_to_turn_2',
        'unit_1_2',
        'time_turn_2_to_turn_3',
        'unit_2_3',
        'time_turn_3_to_turn_4',
        'unit_3_4',
        'time_turn_4_to_turn_5',
        'unit_4_5',
        'time_turn_5_to_turn_6',
        'unit_5_6',
        'time_turn_6_to_turn_7',
        'unit_6_7',
        'time_turn_7_to_turn_8',
        'unit_7_8',
        'time_turn_8_to_turn_9',
        'unit_8_9',
        'time_turn_9_to_turn_10',
        'unit_9_10'
    ];

}
