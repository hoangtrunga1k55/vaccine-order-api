<?php

namespace App\Models;

class Popup extends MongoModel
{
    protected $collection = 'popup';
    protected $guarded = [];

    public function news_info() {
        return $this->belongsTo(News::class,'post_id');
    }
}
