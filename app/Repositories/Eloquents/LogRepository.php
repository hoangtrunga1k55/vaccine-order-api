<?php

namespace App\Repositories\Eloquents;

use App\Models\Log;
use App\Repositories\Contracts\LogRepositoryInterface;

class LogRepository extends BaseRepository implements LogRepositoryInterface
{
    function __construct(Log $model)
    {
        $this->model = $model;
    }
}
