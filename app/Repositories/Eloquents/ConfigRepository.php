<?php

namespace App\Repositories\Eloquents;

use App\Models\Config;
use App\Repositories\Contracts\ConfigRepositoryInterface;

class ConfigRepository extends BaseRepository implements ConfigRepositoryInterface
{
    function __construct(Config $model)
    {
        $this->model = $model;
    }
}
