<?php

namespace App\Repositories\Eloquents;

use App\Models\Ward;
use App\Repositories\Contracts\WardRepositoryInterface;

class WardRepository extends BaseRepository implements WardRepositoryInterface
{
    function __construct(Ward $model)
    {
        $this->model = $model;
    }
}
