<?php

namespace App\Repositories\Eloquents;

use App\Models\District;
use App\Repositories\Contracts\DistrictRepositoryInterface;

class DistrictRepository extends BaseRepository implements DistrictRepositoryInterface
{
    function __construct(District $model)
    {
        $this->model = $model;
    }
}
