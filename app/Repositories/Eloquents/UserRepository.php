<?php

namespace App\Repositories\Eloquents;

use App\Models\Wallet\User;
use App\Repositories\Contracts\UserRepositoryInterface;

class UserRepository extends BaseRepository implements UserRepositoryInterface
{
    function __construct(User $model)
    {
        $this->model = $model;
    }
}
