<?php

namespace App\Repositories\Eloquents;

use App\Models\Pathogen;
use App\Repositories\Contracts\PathogenRepositoryInterface;

class PathogenRepository extends BaseRepository implements PathogenRepositoryInterface
{
    function __construct(Pathogen $model)
    {
        $this->model = $model;
    }

}
