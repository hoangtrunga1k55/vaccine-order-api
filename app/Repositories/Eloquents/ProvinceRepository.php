<?php

namespace App\Repositories\Eloquents;

use App\Models\Province;
use App\Repositories\Contracts\ProvinceRepositoryInterface;

class ProvinceRepository extends BaseRepository implements ProvinceRepositoryInterface
{
    function __construct(Province $model)
    {
        $this->model = $model;
    }
}
