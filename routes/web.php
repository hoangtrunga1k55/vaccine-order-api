<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

use App\Http\Controllers\OrderController;

$router->get('/', function () use ($router) {
    return response()->json([
        'appName' => env("APP_NAME"),
        'appVersion' => env("APP_VERSION")
    ]);
});
$router->group([
    'prefix' => 'api'
], function () use ($router) {
    $router->get('provinces', [
        "as" => "province.get",
        "uses" => "ProvinceController@getProvinces"
    ]);
    $router->get('districts', [
        "as" => "district.get",
        "uses" => "DistrictController@getDistricts"
    ]);

    $router->get('wards', [
        "as" => "ward.get",
        "uses" => "WardController@getWards"
    ]);

    $router->get('location-medicines', [
        "as" => "location-medicine.get",
        "uses" => "LocationMedicineController@getMedicineLocation"
    ]);

    $router->get('pathogens', [
        "as" => "pathogen.get",
        "uses" => "PathogenController@getPathogen"
    ]);

    $router->post('order', [
        "as" => "order.register",
        "uses" => "OrderController@storeRegisterInfo"
    ]);
});
